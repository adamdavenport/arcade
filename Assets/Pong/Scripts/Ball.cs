﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour {

    public float m_Speed = 7.0f;
    public float m_InitialSpeed = 7.0f;
    public float m_MaxSPeed = 12.0f;
    public PongGameManager gameManager;

    private Rigidbody2D m_RigidBody2d;
    private Vector2 m_Velocity;

	// Use this for initialization
	void Start () {
        m_RigidBody2d = GetComponent<Rigidbody2D>();    // Get rigidbody component
        m_Speed = m_InitialSpeed;                       // Set initial speed of the ball
        StartCoroutine(gameManager.StartRound(-1));     // Start the coroutine for the round
	}
	
    /// <summary>
    /// Manage collision
    /// </summary>
    /// <param name="collision"></param>
    void OnCollisionEnter2D(Collision2D collision) {
        string gameObject = collision.gameObject.name;
        // Check if colliding with the left paddle
        if (gameObject == "LeftPaddle") {
            PaddleBounce(collision, 1.0f);
        }
        // Check if colliding with the right paddle
        else if (gameObject == "RightPaddle") {
            PaddleBounce(collision, -1.0f);
        }
        // Check if hitting the top or bottom
        else if (gameObject == "top" || gameObject == "bottom") {
            m_Velocity.y = m_Velocity.y * -1;       // Invert y velocity
            SetVelocity();
        }
        // Determine who scores a point if we hit the left or right wall
        else if (gameObject == "right") {
            gameManager.Score(1);
        }
        else if (gameObject == "left") {
            gameManager.Score(2);
        }
    }

    /// <summary>
    /// Logic for controlling direction the ball will bounce off of a paddle
    /// </summary>
    void PaddleBounce(Collision2D collision, float xVelocity) {
        // Determine where the paddle was hit
        float yVelocity = (transform.position.y - collision.transform.position.y) / collision.collider.bounds.size.y;
        m_Velocity = new Vector2(xVelocity, yVelocity);
        m_Velocity.Normalize();
        SetVelocity();
    }

    // Easily set the velocity of the ball
    void SetVelocity() {
        m_RigidBody2d.velocity = m_Velocity * m_Speed;
    }

    /// <summary>
    /// Reverse direction
    /// </summary>
    void Reverse() {
        m_Velocity = m_Velocity * -1;
        m_RigidBody2d.velocity = m_Velocity;
    }

    // Reset ball position after score
    public void ResetBall(int xVelocity) {
        transform.position = new Vector3(0, 0, 0);
        m_Velocity = new Vector2(xVelocity, 0);
        SetVelocity();
    }
}
