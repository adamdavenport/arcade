﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ComputerPaddle : MonoBehaviour {

    public Transform m_BallTransform;

    private float m_PaddleRadius;
    private Rigidbody2D m_RigidBody;

	// Use this for initialization
	void Start () {
        // Get half the height of the 
        m_PaddleRadius = (GetComponent<BoxCollider2D>().size.y / 2);
        m_RigidBody = GetComponent<Rigidbody2D>();
	}
	
	// Update is called once per frame
	void FixedUpdate () {
        Movement();
	}

    // Control movement logic
    void Movement() {
        // Set speed to 0
        m_RigidBody.velocity = new UnityEngine.Vector2(0, 0);
        float yPosition = transform.position.y;
        float ballPosition = m_BallTransform.position.y;
        // Check if the ball is outside the center half of the paddle and then move it
        if (!(ballPosition < yPosition + m_PaddleRadius / 2 && ballPosition > yPosition - m_PaddleRadius / 2)) {
            if (m_BallTransform.position.y >= transform.position.y + m_PaddleRadius) {
                m_RigidBody.velocity = new UnityEngine.Vector2(0, 2);
            }
            else if (m_BallTransform.position.y <= transform.position.y - m_PaddleRadius) {
                m_RigidBody.velocity = new UnityEngine.Vector2(0, -2);
            }
        }

    }
}
