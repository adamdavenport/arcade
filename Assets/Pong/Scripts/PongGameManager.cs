﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PongGameManager : MonoBehaviour {

    public Ball m_Ball; // Reference to the ball
    public PaddleMovement m_LeftPaddle;         // Left paddle
    public PaddleMovement m_RightPaddle;        // Right paddle
    public Text m_Player1Score;
    public Text m_Player2Score;
    public int m_MaxGoals = 5;


    // Player scores
    private int m_Player1;
    private int m_Player2;

    // Use this for initialization
    void Start() {
        ResetScores();
    }

    void ResetScores() {
        m_Player1 = 0;
        m_Player2 = 0;
        m_Player1Score.text = "0";
        m_Player2Score.text = "0";
    }

    // Update score
    public void Score(int player) {
        m_Ball.m_Speed++; // Increase the ball speed after each goal
        if (player == 1) {
            m_Player1++;
            if (m_Player1 >= m_MaxGoals) {
                Winner(1);
            }
            else {
                m_Player1Score.text = m_Player1.ToString();
                ResetBall(1);   // Send ball back towards player 2.
            }
        }
        else {
            m_Player2++;
            if (m_Player2 >= m_MaxGoals) {
                Winner(2);
            }
            else {
                m_Player2Score.text = m_Player2.ToString();
                ResetBall(-1);  // Send ball back towards player 1.
            }
        }
    }

    // Set the text box for winner
    void Winner(int player) {
        if (player == 1) {
            m_Player1Score.text = m_Player1 + "\nWinner";
            NewGame(2); // Start a new game passing to player 2 first
        }
        else {
            m_Player2Score.text = m_Player2 + "\nWinner";
            NewGame(1); // Start a new game passing to player 1 first
        }
    }

    // Start a new game after a player wins
    void NewGame(int loser) {
        if (loser == 1) {
            loser = -1;
        }
        else {
            loser = 1;
        }
        StartCoroutine(StartNewGame(loser));
    }

    // Function to stop ball movement
    void StopBall() {
        m_Ball.ResetBall(0);
    }

    // After a player scores reset the ball position
    void ResetBall(int xVelocity) {
        // First set velocity to 0 while we give the player time to reset
        StopBall();
        StartCoroutine(StartRound(xVelocity));
    }

    // Sets delay before return
    public IEnumerator StartRound(int xVelocity) {
        yield return new WaitForSeconds(1.0f);
        m_Ball.ResetBall(xVelocity);
    }

    // Start a new round after 5 seconds
    public IEnumerator StartNewGame(int xVelocity) {
        StopBall();
        m_Ball.ResetSpeed();
        yield return new WaitForSeconds(5.0f);
        m_Ball.ResetBall(xVelocity);
        ResetScores();
        ResetPaddles();
    }

    void ResetPaddles() {
        m_RightPaddle.transform.position = new UnityEngine.Vector3(9, 0, 0);
        m_LeftPaddle.transform.position = new UnityEngine.Vector3(-9, 0, 0);
    }
}