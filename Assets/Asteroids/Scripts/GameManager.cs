﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour {

    public float m_SpawnDelay = 0.25f;
    public Asteroid m_Asteroid;
    
    // Base spawn locations
    private Vector3 m_LeftSpawn = new Vector3(-12, 0, 0);
    private Vector3 m_RightSpawn = new Vector3(12, 0, 0);
    private Vector3 m_TopSpawn = new Vector3(0, 6, 0);
    private Vector3 m_BottomSpawn = new Vector3(0, -6, 0);
    // Spawn management
    private float m_SpawnTime = 1;

    // Use this for initialization
    void Start() {
    }

    // Update is called once per frame
    void Update() {
        Spawn();
    }

    // Spawn management function
    void Spawn() {
        m_SpawnTime += Time.deltaTime;
        if (m_SpawnTime >= m_SpawnDelay) {
            int spawnSpot = Random.Range(0, 3); // Randomly choose our spawn
            m_SpawnTime = 0;    // Reset timer
            switch (spawnSpot) {
                case 0:
                    HorizontalSpawn(m_TopSpawn);
                    break;
                case 1:
                    HorizontalSpawn(m_BottomSpawn);
                    break;
                case 2:
                    VerticalSpawn(m_LeftSpawn);
                    break;
                case 3:
                    VerticalSpawn(m_RightSpawn);
                    break;
            }
        }

    }

    void HorizontalSpawn(Vector3 spawnLocation) {
        int x = Random.Range(-12, 12);  // Random variance in the x axis for spawn location
        Asteroid asteroid = Instantiate(m_Asteroid);    // Spawn the object
        spawnLocation.x = x;
        asteroid.gameObject.transform.position = spawnLocation; // Set the position of the asteroid
    }

    void VerticalSpawn(Vector3 spawnLocation) {
        int y = Random.Range(-6, 6);  // Random variance in the y axis for spawn location
        Asteroid asteroid = Instantiate(m_Asteroid);    // Spawn the object
        spawnLocation.y = y;
        asteroid.gameObject.transform.position = spawnLocation; // Set the position of the asteroid
    }
}
