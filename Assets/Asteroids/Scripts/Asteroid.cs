﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Asteroid : MonoBehaviour {

    public float m_MaxHeight = 3;
    public float m_MaxWidth = 3;
    public float m_Force = 100.0f;
    private Rigidbody2D m_RigidBody;

    // Use this for initialization
    void Start() {
        m_RigidBody = GetComponent<Rigidbody2D>();
        SetDirection();
    }

    // Set what direction to move the asteroid
    void SetDirection() {
        // Set random target for the asteroid
        float randomX = Random.Range(-1 * m_MaxWidth *10, m_MaxWidth * 10) / 10;
        float randomY = Random.Range(-1 * m_MaxHeight *10, m_MaxHeight * 10) / 10;

        Vector2 target = new Vector2(randomX, randomY);
        Vector2 velocityVector = target - new Vector2(transform.position.x, transform.position.y);
        velocityVector.Normalize();
        velocityVector = velocityVector * m_Force;
        //m_RigidBody.velocity = velocityVector;
        m_RigidBody.AddForce(velocityVector);
    }

    // Update is called once per frame
    void Update() {

    }
}
