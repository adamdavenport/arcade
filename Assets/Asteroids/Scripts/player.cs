﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour {

    public float m_RotateSpeed;
    public Transform m_FireTransform;
    public Projectile m_Projectile;
    public float m_ProjectileSpeed = 1500;
    public float m_ShootSpeed = 0.1f;

    private string m_InputAxis = "Horizontal";
    private float m_ShotTimer = 0;

    // Use this for initialization
    void Start() {

    }

    // Update is called once per frame
    void FixedUpdate() {
        Turn(); // Manage player rotation
        Fire(); // Manage shooting
    }

    // Control the player's rotation
    void Turn() {
        float rotateAmount = Input.GetAxis(m_InputAxis);    // Store input axis amount
        if (rotateAmount != 0) {
            // Calculate how many degrees we are rotating, multiplied by -1 due to how unity rotates game objects
            rotateAmount = rotateAmount * m_RotateSpeed * Time.deltaTime * -1;
            Vector3 rotateVector = new Vector3(0, 0, rotateAmount); // Create new vector
            transform.Rotate(rotateVector); // Apply rotation
        }
    }

    // Shooting logic
    void Fire() {
        m_ShotTimer += Time.deltaTime;  // Add change in time to timer
        if (Input.GetButton("Fire") && m_ShotTimer >= m_ShootSpeed) {
            m_ShotTimer = 0;                                                // Reset Timer
            Projectile projectile = Instantiate(m_Projectile);              // Spawn projectile
            projectile.transform.position = m_FireTransform.position;       // Set projectile position
            Vector2 velocityVector =  m_FireTransform.position - transform.position;    // Create vector that we will use to add force
            velocityVector.Normalize();                                     // Normalize vector
            velocityVector = velocityVector * m_ProjectileSpeed;            // Add speed to vector
            projectile.GetComponent<Rigidbody2D>().AddForce(velocityVector);// Apply force to rigidbody
        }
    }
}
