﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bounds : MonoBehaviour {

    private void OnTriggerExit2D(Collider2D collision) {
        // Destroy object when it leaves the game zone
        Destroy(collision.gameObject);
    }
}